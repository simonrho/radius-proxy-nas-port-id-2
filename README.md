### Radius proxy

[![N|Solid](https://www.juniper.net/assets/img/logos/juniper/juniper-networks-white-s.png)](https://www.juniper.net/us/en/)

#### What?
This script is a radius proxy translating the NAS-PORT-ID(87) value in new format.

```
SVLAN + zero filled CVLAN (of length 4) + ':' + SVLAN + '-' + SVLAN  
```

#### Examples

| Original value                         | New value                 |
|----------------------------------------|---------------------------|
| "xe-0/1/0.demux0.3221225488:100-200"   |  "1000200:100-200"        |
| "xe-0/1/0.demux0.3221225489:100-99"    |  "1000099:100-99"         |
| "xe-0/1/0.demux0.3221225490:50-5"      |  "500005:50-5"            |


#### radius message support matrix

| code                             | supported? |
|----------------------------------|------------|
| access-request                   | yes        |
| accounting-request start         | yes        |
| accounting-interim start         | yes        |
| coa request                      | no         |
| disconnect request               | no         |



![Radius_proxy_role](radius_proxy_role.jpg)

#### Input arguments format
The radius proxy application supports two options.
1. -s : source address of radius packet sent to radius server. if this value is not defined, the script will derive packet src address from proxy mapping table.
2. -t : timeout value for customer's radius server response. Default value is 3 seconds.
3. -m : radius proxy mapping entries. multiple entries can be specified with comma separator (',')

>   \<local ip>:\<local port>-<remote ip>:\<remote port>-\<radius secret>, ...
   
  The number of mapping entry is unlimited.
  You can copy jrp.py and rename it and use it if you want to have application redundancy.
  For instance, 
  > jrp1.py for 1.1.1.1:11812-10.0.0.1:1812-juniper<br/>
  > jrp2.py for 1.1.1.1:11813-10.0.0.1:1813-juniper<br/>
  > jrp3.py for 1.1.1.1:21812-10.0.0.1:1812-juniper<br/>
  > jrp4.py for 1.1.1.1:21813-10.0.0.1:1813-juniper
  
  The radius secret key can be a <strong>plain string</strong> or <strong>JUNOS encrypted format</strong> value (starting with $9$ prefix)
  You can copy JUNOS encrypted secret key from radius-server configuration on Junos configuration mode CLI
 
#### installation steps
1. copy jrp.py into MX BNG system.
2. register jrp.py file into JET application with radius proxy mapping arguments.
3. add/update JUNOS radius client configuration pointing new radius proxy server listen ip and port. 

```sh
$ scp jrp.py user@bng1:
$ ssh user@bng1
Last login: Mon Mar 22 03:41:04 2021 from 10.107.36.101
--- JUNOS 20.2R1.10 Kernel 64-bit  JNPR-11.0-20200608.0016468_buil
user@bng1> 
user@bng1> request system scripts refresh-from extension-service file jrp.py url /var/home/user/jrp.py 
refreshing 'jrp.py' from '/var/home/user/jrp.py'
user@bng1> 
user@bng1> file list /var/db/scripts/jet/jrp.py detail 
-rw-r-----  1 root  wheel       9283 Mar 22  03:41 /var/db/scripts/jet/jrp.py
total files: 1

user@bng1>
user@bng1> edit 
Entering configuration mode
The configuration has been changed but not committed

[edit]
user@bng1# show system 
scripts {
    language python3;
}
extensions {
    providers {
        jnpr {
            license-type juniper deployment-scope commercial;
        }
    }
    extension-service {
        application {
            file jrp.py {
                arguments "-s 1.1.1.1 -m 1.1.1.1:1812-10.0.0.1:1812-$9$JfUi.QF/0BEP5BEcyW8ZUj,1.1.1.1:1813-10.0.0.1:1813-$9$JfUi.QF/0BEP5BEcyW8ZUj";
                daemonize;
                respawn-on-normal-exit;
                username root;
            }
        }
    }
}

[edit]
user@bng1# show interfaces lo0 
unit 0 {
    family inet {
        address 1.1.1.1/32;
    }
}

[edit]
user@bng1# show access radius-server 
1.1.1.1 {
    port 1812;
    accounting-port 1813;
    secret "$9$JfUi.QF/0BEP5BEcyW8ZUj"; ## SECRET-DATA
    timeout 1;
    retry 3;
    max-outstanding-requests 500;
    source-address 1.1.1.1;
}

[edit]
user@bng1# show access profile freeradius 
authentication-order radius;
radius {
    authentication-server 1.1.1.1;
    accounting-server 1.1.1.1;
}
accounting {
    order radius;
}

[edit]

```


#### radius message support matrix
| code                             | supported? |
|----------------------------------|------------|
| access-request                   | yes        |
| accounting-request start         | yes        |
| accounting-interim start         | yes        |
| coa request                      | no         |
| disconnect request               | no         |


#### freeradius server accounting data
```sh
Thu Jun 17 07:39:19 2021
	Acct-Status-Type = Accounting-On
	Acct-Session-Id = "\000\000\000"
	Event-Timestamp = "Jun 17 2021 15:41:01 UTC"
	Acct-Delay-Time = 0
	Acct-Authentic = RADIUS
	NAS-IP-Address = 1.1.1.1
	NAS-Identifier = "alpha"
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "c0cb5f0fcf239ab3d9c1fcd31fff1efc"
	Timestamp = 1623915559

Thu Jun 17 07:40:21 2021
	User-Name = "0800.2772.0cbe"
	Acct-Status-Type = Start
	Acct-Session-Id = "22"
	Event-Timestamp = "Jun 17 2021 15:42:03 UTC"
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Attr-26.4874.177 = 0x506f72742073706565643a2031303030303030306b
	Acct-Authentic = RADIUS
	ERX-Dhcp-Options = 0x3501013204cb0000650c07636c69656e7431370d011c02030f06770c2c2f1a792a
	ERX-Dhcp-Mac-Addr = "0800.2772.0cbe"
	Framed-IP-Address = 203.0.0.101
	Framed-IP-Netmask = 255.255.255.0
	NAS-Identifier = "alpha"
	NAS-Port = 200
	NAS-Port-Id = "1000200:100-200"
	NAS-Port-Type = Ethernet
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "pppoe 08:00:27:72:0c:be"
	Attr-26.4874.189 = 0xcb000001
	Attr-26.4874.210 = 0x00000004
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "b1f0f47f3d207d86dcf5b09a6d7f708c"
	Timestamp = 1623915621

Thu Jun 17 07:40:33 2021
	User-Name = "0800.2772.0cbe"
	Acct-Status-Type = Stop
	Acct-Session-Id = "22"
	Event-Timestamp = "Jun 17 2021 15:42:15 UTC"
	Acct-Input-Octets = 328
	Acct-Output-Octets = 0
	Acct-Session-Time = 12
	Acct-Input-Packets = 1
	Acct-Output-Packets = 1
	Acct-Terminate-Cause = User-Request
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Acct-Authentic = RADIUS
	ERX-Dhcp-Options = 0x3501013204cb0000650c07636c69656e7431370d011c02030f06770c2c2f1a792a
	ERX-Dhcp-Mac-Addr = "0800.2772.0cbe"
	Framed-IP-Address = 203.0.0.101
	Framed-IP-Netmask = 255.255.255.0
	ERX-Input-Gigapkts = 0
	Acct-Input-Gigawords = 0
	NAS-Identifier = "alpha"
	NAS-Port = 200
	NAS-Port-Id = "1000200:100-200"
	NAS-Port-Type = Ethernet
	ERX-Output-Gigapkts = 0
	Acct-Output-Gigawords = 0
	ERX-IPv6-Acct-Input-Octets = 0
	ERX-IPv6-Acct-Output-Octets = 0
	ERX-IPv6-Acct-Input-Packets = 0
	ERX-IPv6-Acct-Output-Packets = 0
	ERX-IPv6-Acct-Input-Gigawords = 0
	ERX-IPv6-Acct-Output-Gigawords = 0
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "pppoe 08:00:27:72:0c:be"
	Attr-26.4874.189 = 0xcb000001
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "b1f0f47f3d207d86dcf5b09a6d7f708c"
	Timestamp = 1623915633

Thu Jun 17 07:40:43 2021
	User-Name = "juniper"
	Acct-Status-Type = Start
	Acct-Session-Id = "24"
	Event-Timestamp = "Jun 17 2021 15:42:26 UTC"
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Protocol = PPP
	Attr-26.4874.177 = 0x506f72742073706565643a2031303030303030306b
	Acct-Authentic = RADIUS
	ERX-Dhcp-Mac-Addr = "0800.2772.0cbe"
	Framed-IP-Address = 203.0.0.100
	Framed-IP-Netmask = 255.255.255.255
	NAS-Identifier = "alpha"
	NAS-Port = 200
	NAS-Port-Id = "1000200:100-200"
	NAS-Port-Type = Ethernet
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "pppoe 08:00:27:72:0c:be"
	Attr-26.4874.210 = 0x00000004
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "9986f0459282225318b13c89b9b8de7a"
	Timestamp = 1623915643

Thu Jun 17 07:40:53 2021
	User-Name = "juniper"
	Acct-Status-Type = Stop
	Acct-Session-Id = "24"
	Event-Timestamp = "Jun 17 2021 15:42:36 UTC"
	Acct-Input-Octets = 46
	Acct-Output-Octets = 36
	Acct-Session-Time = 10
	Acct-Input-Packets = 1
	Acct-Output-Packets = 1
	Acct-Terminate-Cause = User-Request
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Protocol = PPP
	Acct-Authentic = RADIUS
	ERX-Dhcp-Mac-Addr = "0800.2772.0cbe"
	Framed-IP-Address = 203.0.0.100
	Framed-IP-Netmask = 255.255.255.255
	ERX-Input-Gigapkts = 0
	Acct-Input-Gigawords = 0
	NAS-Identifier = "alpha"
	NAS-Port = 200
	NAS-Port-Id = "1000200:100-200"
	NAS-Port-Type = Ethernet
	ERX-Output-Gigapkts = 0
	Acct-Output-Gigawords = 0
	ERX-IPv6-Acct-Input-Octets = 0
	ERX-IPv6-Acct-Output-Octets = 0
	ERX-IPv6-Acct-Input-Packets = 0
	ERX-IPv6-Acct-Output-Packets = 0
	ERX-IPv6-Acct-Input-Gigawords = 0
	ERX-IPv6-Acct-Output-Gigawords = 0
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "pppoe 08:00:27:72:0c:be"
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "9986f0459282225318b13c89b9b8de7a"
	Timestamp = 1623915653



```
#### Radius proxy log messages
```shell
tail -f /var/log/jrp.log
2021-06-17 15:33:56.059 INFO 427: **********************
2021-06-17 15:33:56.059 INFO 428: radius proxy starts!!!
2021-06-17 15:33:56.059 INFO 429: **********************
2021-06-17 15:33:56.060 INFO 430: python version: 3.7.3
2021-06-17 15:33:56.204 DEBUG 432: arguments: ['/var/run/scripts/jet//jrp.py', '-s', '1.1.1.1', '-m', '127.0.1.1:1812-10.0.0.1:1812-$9$rlFeKWLXNdVs,127.0.1.1:1813-10.0.0.1:1813-$9$rlFeKWLXNdVs,127.0.1.2:1812-10.0.0.1:1812-$9$rlFeKWLXNdVs,127.0.1.2:1813-10.0.0.1:1813-$9$rlFeKWLXNdVs']
2021-06-17 15:33:56.206 DEBUG 452: proxy mapping: 127.0.1.1:1812 <==> 10.0.0.1:1812 : $9$rlFeKWLXNdVs
2021-06-17 15:33:56.208 DEBUG 452: proxy mapping: 127.0.1.1:1813 <==> 10.0.0.1:1813 : $9$rlFeKWLXNdVs
2021-06-17 15:33:56.208 DEBUG 452: proxy mapping: 127.0.1.2:1812 <==> 10.0.0.1:1812 : $9$rlFeKWLXNdVs
2021-06-17 15:33:56.209 DEBUG 452: proxy mapping: 127.0.1.2:1813 <==> 10.0.0.1:1813 : $9$rlFeKWLXNdVs
2021-06-17 15:33:56.210 DEBUG 468: proxy packet source address: 1.1.1.1
2021-06-17 15:34:13.406 DEBUG 363: accounting-request  id: 93 nas-port-id translated: "xe-0/1/0.demux0.3221225484:100-200" -> "1000200:100-200"
2021-06-17 15:34:18.606 DEBUG 363: access-request      id: 94 nas-port-id translated: "xe-0/1/0.demux0.3221225486:100-200" -> "1000200:100-200"
2021-06-17 15:34:18.806 DEBUG 363: accounting-request  id: 95 nas-port-id translated: "xe-0/1/0.demux0.3221225486:100-200" -> "1000200:100-200"
2021-06-17 15:37:52.706 DEBUG 363: accounting-request  id: 96 nas-port-id translated: "xe-0/1/0.demux0.3221225486:100-200" -> "1000200:100-200"
2021-06-17 15:39:24.814 DEBUG 363: access-request      id: 2 nas-port-id translated: "xe-0/1/0.demux0.3221225488:100-200" -> "1000200:100-200"
2021-06-17 15:39:28.222 DEBUG 363: access-request      id: 3 nas-port-id translated: "xe-0/1/0.demux0.3221225488:100-200" -> "1000200:100-200"
2021-06-17 15:39:32.631 DEBUG 363: access-request      id: 4 nas-port-id translated: "xe-0/1/0.demux0.3221225488:100-200" -> "1000200:100-200"
2021-06-17 15:42:03.598 DEBUG 363: access-request      id: 2 nas-port-id translated: "xe-0/1/0.demux0.3221225489:100-200" -> "1000200:100-200"
2021-06-17 15:42:03.799 DEBUG 363: accounting-request  id: 3 nas-port-id translated: "xe-0/1/0.demux0.3221225489:100-200" -> "1000200:100-200"
2021-06-17 15:42:15.422 DEBUG 363: accounting-request  id: 4 nas-port-id translated: "xe-0/1/0.demux0.3221225489:100-200" -> "1000200:100-200"
2021-06-17 15:42:25.806 DEBUG 363: access-request      id: 5 nas-port-id translated: "xe-0/1/0.demux0.3221225491:100-200" -> "1000200:100-200"
2021-06-17 15:42:26.106 DEBUG 363: accounting-request  id: 6 nas-port-id translated: "xe-0/1/0.demux0.3221225491:100-200" -> "1000200:100-200"
2021-06-17 15:42:35.806 DEBUG 363: accounting-request  id: 7 nas-port-id translated: "xe-0/1/0.demux0.3221225491:100-200" -> "1000200:100-200"

```

#### software version
##### JUNOS
```sh
user@bng1# run show version 
Hostname: alpha
Model: mx204
Junos: 21.1R1.11
JUNOS OS Kernel 64-bit  [20210308.e5f5942_builder_stable_11]
JUNOS OS libs [20210308.e5f5942_builder_stable_11]
JUNOS OS runtime [20210308.e5f5942_builder_stable_11]
JUNOS OS time zone information [20210308.e5f5942_builder_stable_11]
JUNOS network stack and utilities [20210318.191732_builder_junos_211_r1]
JUNOS libs [20210318.191732_builder_junos_211_r1]
JUNOS OS libs compat32 [20210308.e5f5942_builder_stable_11]
JUNOS OS 32-bit compatibility [20210308.e5f5942_builder_stable_11]
JUNOS libs compat32 [20210318.191732_builder_junos_211_r1]
JUNOS runtime [20210318.191732_builder_junos_211_r1]
Junos vmguest package [20210318.191732_builder_junos_211_r1]
JUNOS sflow mx [20210318.191732_builder_junos_211_r1]
JUNOS py extensions [20210318.191732_builder_junos_211_r1]
JUNOS py base [20210318.191732_builder_junos_211_r1]
JUNOS OS vmguest [20210308.e5f5942_builder_stable_11]
JUNOS OS crypto [20210308.e5f5942_builder_stable_11]
JUNOS OS boot-ve files [20210308.e5f5942_builder_stable_11]
JUNOS na telemetry [21.1R1.11]
JUNOS Security Intelligence [20210318.191732_builder_junos_211_r1]
JUNOS mx libs compat32 [20210318.191732_builder_junos_211_r1]
JUNOS mx runtime [20210318.191732_builder_junos_211_r1]
JUNOS RPD Telemetry Application [21.1R1.11]
JUNOS Routing mpls-oam-basic [20210318.191732_builder_junos_211_r1]
JUNOS Routing mpls-oam-advanced [20210318.191732_builder_junos_211_r1]
JUNOS Routing lsys [20210318.191732_builder_junos_211_r1]
JUNOS Routing controller-internal [20210318.191732_builder_junos_211_r1]
JUNOS Routing controller-external [20210318.191732_builder_junos_211_r1]
JUNOS Routing 32-bit Compatible Version [20210318.191732_builder_junos_211_r1]
JUNOS Routing aggregated [20210318.191732_builder_junos_211_r1]
Redis [20210318.191732_builder_junos_211_r1]
JUNOS probe utility [20210318.191732_builder_junos_211_r1]
JUNOS common platform support [20210318.191732_builder_junos_211_r1]
JUNOS Openconfig [21.1R1.11]
JUNOS mtx network modules [20210318.191732_builder_junos_211_r1]
JUNOS modules [20210318.191732_builder_junos_211_r1]
JUNOS mx modules [20210318.191732_builder_junos_211_r1]
JUNOS mx libs [20210318.191732_builder_junos_211_r1]
JUNOS SQL Sync Daemon [20210318.191732_builder_junos_211_r1]
JUNOS mtx Data Plane Crypto Support [20210318.191732_builder_junos_211_r1]
JUNOS daemons [20210318.191732_builder_junos_211_r1]
JUNOS mx daemons [20210318.191732_builder_junos_211_r1]
JUNOS Broadband Egde User Plane Apps [21.1R1.11]
JUNOS appidd-mx application-identification daemon [20210318.191732_builder_junos_211_r1]
JUNOS TPM2 [20210318.191732_builder_junos_211_r1]
JUNOS Services URL Filter package [20210318.191732_builder_junos_211_r1]
JUNOS Services TLB Service PIC package [20210318.191732_builder_junos_211_r1]
JUNOS Services Telemetry [20210318.191732_builder_junos_211_r1]
JUNOS Services TCP-LOG [20210318.191732_builder_junos_211_r1]
JUNOS Services SSL [20210318.191732_builder_junos_211_r1]
JUNOS Services SOFTWIRE [20210318.191732_builder_junos_211_r1]
JUNOS Services Stateful Firewall [20210318.191732_builder_junos_211_r1]
JUNOS Services RTCOM [20210318.191732_builder_junos_211_r1]
JUNOS Services RPM [20210318.191732_builder_junos_211_r1]
JUNOS Services PCEF package [20210318.191732_builder_junos_211_r1]
JUNOS Services NAT [20210318.191732_builder_junos_211_r1]
JUNOS Services Mobile Subscriber Service Container package [20210318.191732_builder_junos_211_r1]
JUNOS Services MobileNext Software package [20210318.191732_builder_junos_211_r1]
JUNOS Services Logging Report Framework package [20210318.191732_builder_junos_211_r1]
JUNOS Services LL-PDF Container package [20210318.191732_builder_junos_211_r1]
JUNOS Services Jflow Container package [20210318.191732_builder_junos_211_r1]
JUNOS Services Deep Packet Inspection package [20210318.191732_builder_junos_211_r1]
JUNOS Services IPSec [20210318.191732_builder_junos_211_r1]
JUNOS Services IDS [20210318.191732_builder_junos_211_r1]
JUNOS IDP Services [20210318.191732_builder_junos_211_r1]
JUNOS Services HTTP Content Management package [20210318.191732_builder_junos_211_r1]
JUNOS Services DNS Filter package (i386) [20210318.191732_builder_junos_211_r1] 
JUNOS Services Crypto [20210318.191732_builder_junos_211_r1]
JUNOS Services Captive Portal and Content Delivery Container package [20210318.191732_builder_junos_211_r1]
JUNOS Services COS [20210318.191732_builder_junos_211_r1]
JUNOS AppId Services [20210318.191732_builder_junos_211_r1]
JUNOS Services Application Level Gateways [20210318.191732_builder_junos_211_r1]
JUNOS Services AACL Container package [20210318.191732_builder_junos_211_r1]
JUNOS SDN Software Suite [20210318.191732_builder_junos_211_r1]
JUNOS Extension Toolkit [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (wrlinux9) [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (MX/EX92XX Common) [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (M/T Common) [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (aft) [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (MX Common) [20210318.191732_builder_junos_211_r1]
JUNOS Juniper Malware Removal Tool (JMRT) [1.0.0+20210318.191732_builder_junos_211_r1]
JUNOS J-Insight [20210318.191732_builder_junos_211_r1]
JUNOS jfirmware [20210318.191732_builder_junos_211_r1]
JUNOS Online Documentation [20210318.191732_builder_junos_211_r1]
JUNOS jail runtime [20210308.e5f5942_builder_stable_11]


```

##### Python
```sh
python3.7
```